#-------------------------------------------------
#
# Project created by QtCreator 2024-01-15T13:42:53
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjectMultiScreen
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    secondarywindow.cpp \
    GoogleInput/input.cpp \
    GoogleInput/keyboard.cpp \
    GoogleInput/keybutton.cpp \
    GoogleInput/googlepinyin/dictbuilder.cpp \
    GoogleInput/googlepinyin/dictlist.cpp \
    GoogleInput/googlepinyin/dicttrie.cpp \
    GoogleInput/googlepinyin/lpicache.cpp \
    GoogleInput/googlepinyin/matrixsearch.cpp \
    GoogleInput/googlepinyin/mystdlib.cpp \
    GoogleInput/googlepinyin/ngram.cpp \
    GoogleInput/googlepinyin/pinyinime.cpp \
    GoogleInput/googlepinyin/searchutility.cpp \
    GoogleInput/googlepinyin/spellingtable.cpp \
    GoogleInput/googlepinyin/spellingtrie.cpp \
    GoogleInput/googlepinyin/splparser.cpp \
    GoogleInput/googlepinyin/sync.cpp \
    GoogleInput/googlepinyin/userdict.cpp \
    GoogleInput/googlepinyin/utf16char.cpp \
    GoogleInput/googlepinyin/utf16reader.cpp \
    winmusicitem.cpp \
    winrecommenditem.cpp \
    winrecommendtitle.cpp

HEADERS += \
        mainwindow.h \
    secondarywindow.h \
    GoogleInput/input.h \
    GoogleInput/keyboard.h \
    GoogleInput/keybutton.h \
    GoogleInput/googlepinyin/atomdictbase.h \
    GoogleInput/googlepinyin/dictbuilder.h \
    GoogleInput/googlepinyin/dictdef.h \
    GoogleInput/googlepinyin/dictlist.h \
    GoogleInput/googlepinyin/dicttrie.h \
    GoogleInput/googlepinyin/lpicache.h \
    GoogleInput/googlepinyin/matrixsearch.h \
    GoogleInput/googlepinyin/mystdlib.h \
    GoogleInput/googlepinyin/ngram.h \
    GoogleInput/googlepinyin/pinyinime.h \
    GoogleInput/googlepinyin/searchutility.h \
    GoogleInput/googlepinyin/spellingtable.h \
    GoogleInput/googlepinyin/spellingtrie.h \
    GoogleInput/googlepinyin/splparser.h \
    GoogleInput/googlepinyin/sync.h \
    GoogleInput/googlepinyin/userdict.h \
    GoogleInput/googlepinyin/utf16char.h \
    GoogleInput/googlepinyin/utf16reader.h \
    winmusicitem.h \
    winrecommenditem.h \
    winrecommendtitle.h

FORMS += \
        mainwindow.ui \
    secondarywindow.ui \
    GoogleInput/keyboard.ui \
    winmusicitem.ui \
    winrecommenditem.ui \
    winrecommendtitle.ui

RESOURCES += \
    pic.qrc

SUBDIRS += \
    GoogleInput/googlepinyin/googlepinyin.pro
