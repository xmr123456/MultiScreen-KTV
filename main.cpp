#include "mainwindow.h"
#include "secondarywindow.h"
#include <QApplication>
#include <QDebug>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QDialog>
#include <QObject>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow main_w;
    SecondaryWindow second_w;
    QDesktopWidget* desktopWidget = QApplication::desktop();
    main_w.setGeometry(desktopWidget->screenGeometry(0));
    main_w.showFullScreen();

    second_w.setGeometry(desktopWidget->screenGeometry(1));
    second_w.showFullScreen();

    QObject::connect(&main_w,SIGNAL(sign_music(QString)),&second_w,SLOT(slot_music(QString)));
    QObject::connect(&main_w,SIGNAL(sign_progress(int)),&second_w,SLOT(slot_progress(int)));
    QObject::connect(&main_w,SIGNAL(sign_audio(int)),&second_w,SLOT(slot_audio(int)));
    QObject::connect(&main_w,SIGNAL(sign_mute(bool)),&second_w,SLOT(slot_mute(bool)));
    QObject::connect(&main_w,SIGNAL(sign_front()),&second_w,SLOT(slot_front()));
    QObject::connect(&main_w,SIGNAL(sign_stop()),&second_w,SLOT(slot_stop()));
    QObject::connect(&main_w,SIGNAL(sign_pause()),&second_w,SLOT(slot_pause()));
    QObject::connect(&main_w,SIGNAL(sign_back()),&second_w,SLOT(slot_back()));
    QObject::connect(&main_w,SIGNAL(sign_start()),&second_w,SLOT(slot_start()));
    QObject::connect(&second_w,SIGNAL(sign_positionChanged(qint64)),&main_w,SLOT(slot_positionChanged(qint64)));
    QObject::connect(&second_w,SIGNAL(sign_durationChanged(qint64)),&main_w,SLOT(slot_durationChanged(qint64)));
    return a.exec();
}
