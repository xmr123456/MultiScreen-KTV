#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    keyBoard = new KeyBoard(this);
    keyBoard->show();
    keyBoard->setStyleSheet("background-color:rgba(128,128,128,128);"
                            "font: 18pt \"微软雅黑\";"
                            "color:rgb(255,255,255);");
    ui->lineEdit->installEventFilter(this);
    winInit();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::winInit()
{
    muteFlag = false;
    playFlag = false;
    currMusic = "";
    nextMusic = "";
    winRecommendTitle = new WinRecommendTitle();
    QDir dir;
    if(!dir.exists(QString("%1/music").arg(QApplication::applicationDirPath())))
        dir.mkpath(QString("%1/music").arg(QApplication::applicationDirPath()));
    dir = QDir(QString("%1/music").arg(QApplication::applicationDirPath()));
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    dir.setSorting(QDir::Time);
    QStringList nameFilterList;
    nameFilterList<<"*.mp4"<<"*.mkv"<<"*.avi"<<"*.wmv"<<"*.rmvb"<<"*.mov";
    QFileInfoList fileList = dir.entryInfoList(nameFilterList);
    foreach (auto item, fileList) {
        allMusicInfo[item.fileName()] = item.filePath();
        recommendMusicInfo[item.fileName()] = item.filePath();
    }
}

void MainWindow::onRecommendDisplay()
{
    ui->listWidgetRecommend->setCurrentRow(-1);
    for(int i = 0;i < ui->listWidgetRecommend->count();i++){
        QListWidgetItem *listItem = ui->listWidgetRecommend->item(i);
        ui->listWidgetRecommend->takeItem(i);
        delete listItem;
        i -= 1;
    }
    foreach(auto item,recommendMusicInfo.keys()){
        QListWidgetItem *widgetItem = new QListWidgetItem;
        widgetItem->setSizeHint(QSize(ui->listWidgetRecommend->width(),80));
        WinRecommendItem *recommendItem = new WinRecommendItem;
        recommendItem->setFixedSize(QSize(ui->listWidgetRecommend->width(),80));

        QString suffixStr = "." + item.split(".").last();
        QString filename = item;
        filename = filename.replace(suffixStr,"");
        if(selectMusicInfo.keys().contains(item))
            recommendItem->setAddVisible(false);
        recommendItem->setInfo(item,filename.split("-").last(),filename.split("-").first());
        ui->listWidgetRecommend->addItem(widgetItem);
        ui->listWidgetRecommend->setItemWidget(widgetItem,recommendItem);
        connect(recommendItem,SIGNAL(sign_add(QString)),this,SLOT(slot_add(QString)));
    }
}

void MainWindow::onMusicDisplay()
{
    int currRow,nextRow;
    currRow = nextRow = 0;
    for(int i = 0;i < ui->listWidgetMusic->count();i++){
        QListWidgetItem *item = ui->listWidgetMusic->item(i);
        WinMusicItem *musicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(item);
        if(musicItem->getMusic() == currMusic){
            currRow = i;
            nextRow = (currRow + 1) % ui->listWidgetMusic->count();
            musicItem->setPlay(true);
            musicItem->setPause(playFlag);
        }
        else{
            musicItem->setPlay(false);
            musicItem->setPause(false);
        }
    }

    if(currMusic != ""){
        QListWidgetItem *nextItem = ui->listWidgetMusic->item(nextRow);
        WinMusicItem *nextMusicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(nextItem);
        nextMusic = nextMusicItem->getMusic();
    }
    ui->label_curr->setText(currMusic);
    ui->label_next->setText(nextMusic);
}

void MainWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    keyBoard->setFixedWidth(ui->listWidgetRecommend->width());
    keyBoard->setGeometry(ui->widget_6->x() + 20,ui->widget_6->height() - keyBoard->height() + 20,keyBoard->width(),keyBoard->height());
    keyBoard->hide();
    onRecommendDisplay();
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if(watched->inherits("QLineEdit") && event->type() == QEvent::MouseButtonRelease){
        QWidget *p = qobject_cast<QWidget *>(watched);
        keyBoard->setEdit(p);
        keyBoard->show();
    }
    return QWidget::eventFilter(watched,event);
}

void MainWindow::slot_stop(QString music)
{
    Q_UNUSED(music)
    emit sign_pause();
    ui->btn_pause->setIcon(QIcon(":/image/button/9.png"));
}

void MainWindow::slot_add(QString music)
{
    selectMusicInfo[music] = allMusicInfo[music];
    for(int i = 0;i < ui->listWidgetRecommend->count();i++){
        QListWidgetItem *item = ui->listWidgetRecommend->item(i);
        WinRecommendItem *recommendItem = (WinRecommendItem *)ui->listWidgetRecommend->itemWidget(item);
        if(recommendItem->getMusic() == music){
            recommendItem->setAddVisible(false);
            break;
        }
    }

    QListWidgetItem *item = new QListWidgetItem;
    item->setSizeHint(QSize(ui->listWidgetMusic->width(),80));
    WinMusicItem *musicItem = new WinMusicItem;
    musicItem->setFixedSize(QSize(ui->listWidgetMusic->width(),80));
    QString suffixStr = "." + music.split(".").last();
    QString filename = music;
    filename = filename.replace(suffixStr,"");
    musicItem->setInfo(music,filename.split("-").last(),filename.split("-").first());
    ui->listWidgetMusic->addItem(item);
    ui->listWidgetMusic->setItemWidget(item,musicItem);

    connect(musicItem,SIGNAL(sign_del(QString)),this,SLOT(slot_del(QString)));
    connect(musicItem,SIGNAL(sign_playing(QString)),this,SLOT(slot_playing(QString)));
    connect(musicItem,SIGNAL(sign_stop(QString)),this,SLOT(slot_stop(QString)));
    ui->label_music_number->setText(QString("%1").arg(selectMusicInfo.keys().length()));
    onMusicDisplay();
}

void MainWindow::slot_del(QString music)
{
    for(int i = 0;i < ui->listWidgetMusic->count();i++){
        QListWidgetItem *item = ui->listWidgetMusic->item(i);
        WinMusicItem *musicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(item);
        if(musicItem->getMusic() == music){
            ui->listWidgetMusic->takeItem(i);
            delete item;
            break;
        }
    }
    selectMusicInfo.remove(music);
    ui->label_music_number->setText(QString("%1").arg(selectMusicInfo.keys().length()));
    if(music == currMusic)
        on_btn_next_clicked();
    onRecommendDisplay();
    onMusicDisplay();
}

void MainWindow::slot_playing(QString music)
{
    currMusic = music;
    ui->btn_pause->setIcon(QIcon(":/image/button/8.png"));
    emit sign_music(selectMusicInfo[music]);
    onMusicDisplay();
}

void MainWindow::slot_positionChanged(qint64 position)
{
    m_position = position;
    int secs = position / 1000;
    ui->label_position->setText(QString("%1:%2").arg(secs / 60,2,10,QLatin1Char('0'))
                                .arg(secs % 60,2,10,QLatin1Char('0')));
    ui->horizontalSlider_progress->setValue(int(((m_position * 1.0) / (m_duration * 1.0)) * 100));
    if(ui->horizontalSlider_progress->value() == 100){
        on_btn_next_clicked();
    }
}
void MainWindow::slot_durationChanged(qint64 duration)
{
    m_duration = duration;
    int secs = duration / 1000;
    ui->label_duration->setText(QString("%1:%2").arg(secs / 60,2,10,QLatin1Char('0'))
                                .arg(secs % 60,2,10,QLatin1Char('0')));
}

void MainWindow::on_horizontalSlider_progress_sliderReleased()
{
    emit sign_progress(ui->horizontalSlider_progress->value());
}

void MainWindow::on_horizontalSlider_audio_sliderMoved(int position)
{
    ui->label_audio->setText(QString("%1").arg(position));
    emit sign_audio(position);
}

void MainWindow::on_btn_audio_clicked()
{
    muteFlag = muteFlag ? false : true;
    emit sign_mute(muteFlag);
    if(muteFlag)
        ui->btn_audio->setIcon(QIcon(":/image/button/12.png"));
    else
        ui->btn_audio->setIcon(QIcon(":/image/button/13.png"));
}

void MainWindow::on_btn_next_clicked()
{
    if(ui->listWidgetMusic->count() == 0){
        currMusic = nextMusic = "";
        emit sign_stop();
        onMusicDisplay();
    }
    else{
        int currRow;
        if(currMusic == ""){
            currRow = 0;
            QListWidgetItem *currItem = ui->listWidgetMusic->item(currRow);
            WinMusicItem *currMusicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(currItem);
            currMusic = currMusicItem->getMusic();
        }
        else
            currMusic = nextMusic;
        emit sign_music(selectMusicInfo[currMusic]);
        playFlag = true;
        ui->btn_pause->setIcon(QIcon(":/image/button/8.png"));
        onMusicDisplay();
    }
}

void MainWindow::on_btn_front_clicked()
{
    emit sign_front();
}

void MainWindow::on_btn_stop_clicked()
{
    if(playFlag)
        on_btn_pause_clicked();
    emit sign_stop();
}

void MainWindow::on_btn_pause_clicked()
{
    if(ui->listWidgetMusic->count() == 0)
        return;
    if(playFlag){
        playFlag = false;
        ui->btn_pause->setIcon(QIcon(":/image/button/9.png"));
        emit sign_pause();
    }
    else{
        playFlag = true;
        ui->btn_pause->setIcon(QIcon(":/image/button/8.png"));
        if(currMusic.length() == 0){
            QListWidgetItem *currItem = ui->listWidgetMusic->item(0);
            WinMusicItem *currMusicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(currItem);
            currMusic = currMusicItem->getMusic();
            emit sign_music(selectMusicInfo[currMusic]);
        }
        else{
            emit sign_start();
        }
    }
    onMusicDisplay();
}

void MainWindow::on_btn_back_clicked()
{
    emit sign_back();
}

void MainWindow::on_btn_prev_clicked()
{
    if(ui->listWidgetMusic->count() == 0){
        currMusic = nextMusic = "";
        emit sign_stop();
        onMusicDisplay();
    }
    else{
        int currRow = 0;
        if(currMusic.length() == 0){
            QListWidgetItem *currItem = ui->listWidgetMusic->item(0);
            WinMusicItem *currMusicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(currItem);
            currMusic = currMusicItem->getMusic();
        }
        else{
            for(int i = 0;i < ui->listWidgetMusic->count();i++){
                QListWidgetItem *item = ui->listWidgetMusic->item(i);
                WinMusicItem *musicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(item);
                if(musicItem->getMusic() == currMusic){
                    if(i == 0)
                        currRow = ui->listWidgetMusic->count() - 1;
                    else
                        currRow = i - 1;
                    break;
                }
            }
            QListWidgetItem *currItem = ui->listWidgetMusic->item(currRow);
            WinMusicItem *currMusicItem = (WinMusicItem *)ui->listWidgetMusic->itemWidget(currItem);
            currMusic = currMusicItem->getMusic();
        }
        emit sign_music(selectMusicInfo[currMusic]);
        playFlag = true;
        ui->btn_pause->setIcon(QIcon(":/image/button/8.png"));
        onMusicDisplay();
    }
}

void MainWindow::on_btn_search_clicked()
{
    QString str = ui->lineEdit->text();
    recommendMusicInfo.clear();
    foreach(auto item,allMusicInfo.keys()){
        if(item.indexOf(str) >= 0){
            recommendMusicInfo[item] = allMusicInfo[item];
        }
    }
    onRecommendDisplay();
}

void MainWindow::on_listWidgetRecommend_currentRowChanged(int currentRow)
{
    for(int i = 0;i < ui->listWidgetRecommend->count();i++){
        QListWidgetItem *item = ui->listWidgetRecommend->item(i);
        WinRecommendItem *recommendItem = (WinRecommendItem *)ui->listWidgetRecommend->itemWidget(item);
        if(i == currentRow){
            recommendItem->setSelect(true);
        }
        else{
            recommendItem->setSelect(false);
        }
    }
}
