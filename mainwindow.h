#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QShowEvent>
#include <QEvent>
#include <QListWidgetItem>
#include <QFile>
#include <QDir>
#include <QDirIterator>
#include <QFileInfoList>
#include <QFileInfo>
#include <QDesktopWidget>
#include <QMap>
#include "secondarywindow.h"
#include "GoogleInput/keyboard.h"
#include "winrecommendtitle.h"
#include "winmusicitem.h"
#include "winrecommenditem.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void winInit();
    void onRecommendDisplay();
    void onMusicDisplay();
protected:
    void showEvent(QShowEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
private slots:
    void slot_stop(QString music);
    void slot_add(QString music);
    void slot_del(QString music);
    void slot_playing(QString music);
    void slot_positionChanged(qint64 position);
    void slot_durationChanged(qint64 duration);
    void on_horizontalSlider_progress_sliderReleased();
    void on_listWidgetRecommend_currentRowChanged(int currentRow);
    void on_horizontalSlider_audio_sliderMoved(int position);
    void on_btn_audio_clicked();
    void on_btn_next_clicked();
    void on_btn_front_clicked();
    void on_btn_stop_clicked();
    void on_btn_pause_clicked();
    void on_btn_back_clicked();
    void on_btn_prev_clicked();
    void on_btn_search_clicked();

signals:
    void sign_music(QString music);
    void sign_progress(int percent);
    void sign_audio(int volume);
    void sign_mute(bool mute);
    void sign_front();
    void sign_stop();
    void sign_pause();
    void sign_start();
    void sign_back();

private:
    Ui::MainWindow *ui;
    KeyBoard *keyBoard;
    WinRecommendTitle *winRecommendTitle;
    QMap<QString,QString> allMusicInfo;
    QMap<QString,QString> recommendMusicInfo;
    QMap<QString,QString> selectMusicInfo;
    QString currMusic;
    QString nextMusic;
    bool muteFlag;
    bool playFlag;
    qint64 m_duration,m_position;
};

#endif // MAINWINDOW_H
