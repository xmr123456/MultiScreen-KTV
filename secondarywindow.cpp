#include "secondarywindow.h"
#include "ui_secondarywindow.h"
#include <QDebug>
SecondaryWindow::SecondaryWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SecondaryWindow)
{
    ui->setupUi(this);
    player = new QMediaPlayer(this);
    videoWidget = new QVideoWidget(ui->widget);
    connect(player,SIGNAL(durationChanged(qint64)),this,SIGNAL(sign_durationChanged(qint64)));
    connect(player,SIGNAL(positionChanged(qint64)),this,SIGNAL(sign_positionChanged(qint64)));
}

SecondaryWindow::~SecondaryWindow()
{
    delete ui;
}

void SecondaryWindow::slot_music(QString media)
{
    if(player->state() == QMediaPlayer::PlayingState){
        player->stop();
    }
    player->setMedia(QMediaContent(QUrl::fromLocalFile(media)));
    player->play();
}

void SecondaryWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    videoWidget->resize(ui->widget->size());
    player->setVideoOutput(videoWidget);
    videoWidget->show();
}

void SecondaryWindow::slot_progress(int percent)
{
    player->setPosition(qint64(((player->duration() * 1.0) / (100 * 1.0)) * percent));
}

void SecondaryWindow::slot_audio(int volume)
{
    player->setVolume(volume);
}

void SecondaryWindow::slot_mute(bool mute)
{
    player->setMuted(mute);
}

void SecondaryWindow::slot_front()
{
    player->setPosition(player->position() + 5000);
}

void SecondaryWindow::slot_stop()
{
    player->stop();
}

void SecondaryWindow::slot_pause()
{
    player->pause();
}

void SecondaryWindow::slot_start()
{
    player->play();
}

void SecondaryWindow::slot_back()
{
    player->setPosition(player->position() - 5000);
}
