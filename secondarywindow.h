#ifndef SECONDARYWINDOW_H
#define SECONDARYWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QShowEvent>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QMediaPlaylist>
#include <QMediaContent>
#include <QUrl>
namespace Ui {
class SecondaryWindow;
}

class SecondaryWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SecondaryWindow(QWidget *parent = 0);
    ~SecondaryWindow();
protected:
    void showEvent(QShowEvent *event);
private slots:
    void slot_music(QString media);
    void slot_progress(int percent);
    void slot_audio(int volume);
    void slot_mute(bool mute);
    void slot_front();
    void slot_stop();
    void slot_pause();
    void slot_start();
    void slot_back();
signals:
    void sign_positionChanged(qint64 position);
    void sign_durationChanged(qint64 duration);
private:
    Ui::SecondaryWindow *ui;
    QMediaPlayer *player;
    QVideoWidget *videoWidget;
};

#endif // SECONDARYWINDOW_H
