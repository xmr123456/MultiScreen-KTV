#include "winmusicitem.h"
#include "ui_winmusicitem.h"

WinMusicItem::WinMusicItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WinMusicItem)
{
    ui->setupUi(this);
    playingFlag = false;
}

WinMusicItem::~WinMusicItem()
{
    delete ui;
}

void WinMusicItem::setInfo(QString music, QString name, QString author)
{
    this->music = music;
    ui->label_name->setText(name);
    ui->label_author->setText(author);
}

QString WinMusicItem::getName()
{
    return ui->label_name->text();
}

QString WinMusicItem::getAuthor()
{
    return ui->label_author->text();
}

QString WinMusicItem::getMusic()
{
    return music;
}

void WinMusicItem::setPlay(bool playFlag)
{
    ui->label_playing->setVisible(playFlag);
    if(playFlag){
        ui->widget->setStyleSheet("#widget{background-color:rgb(50,89,206);}");
    }
    else{
        ui->widget->setStyleSheet("#widget{background-color:rgba(128,128,128,198);}");
    }
}

void WinMusicItem::setPause(bool playFlag)
{
    this->playingFlag = playFlag;
    if(playFlag)
        ui->btn_playing->setIcon(QIcon(":/image/bfzn_stop.png"));
    else
        ui->btn_playing->setIcon(QIcon(":/image/bfzn_begin.png"));
}

void WinMusicItem::on_btn_playing_clicked()
{
    if(!playingFlag){
        playingFlag = true;
        emit sign_playing(music);
        ui->btn_playing->setIcon(QIcon(":/image/bfzn_stop.png"));
    }
    else{
        playingFlag = false;
        emit sign_stop(music);
        ui->btn_playing->setIcon(QIcon(":/image/bfzn_begin.png"));
    }
}

void WinMusicItem::on_btn_del_clicked()
{
    emit sign_del(music);
}
