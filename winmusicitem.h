#ifndef WINMUSICITEM_H
#define WINMUSICITEM_H

#include <QWidget>

namespace Ui {
class WinMusicItem;
}

class WinMusicItem : public QWidget
{
    Q_OBJECT

public:
    explicit WinMusicItem(QWidget *parent = 0);
    ~WinMusicItem();
    void setInfo(QString music,QString name,QString author);
    QString getName();
    QString getAuthor();
    QString getMusic();
    void setPlay(bool playFlag);
    void setPause(bool playFlag);

private slots:
    void on_btn_playing_clicked();
    void on_btn_del_clicked();
signals:
    void sign_playing(QString music);
    void sign_del(QString music);
    void sign_stop(QString music);

private:
    Ui::WinMusicItem *ui;
    QString music;
    bool playingFlag;
};

#endif // WINMUSICITEM_H
