#include "winrecommenditem.h"
#include "ui_winrecommenditem.h"

WinRecommendItem::WinRecommendItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WinRecommendItem)
{
    ui->setupUi(this);
}

WinRecommendItem::~WinRecommendItem()
{
    delete ui;
}

void WinRecommendItem::setInfo(QString music,QString name, QString author)
{
    this->music = music;
    ui->label_name->setText(name);
    ui->label_author->setText(author);
}

void WinRecommendItem::setAddVisible(bool visible)
{
    ui->btn_add->setVisible(visible);
}

QString WinRecommendItem::getName()
{
    return ui->label_name->text();
}

QString WinRecommendItem::getAuthor()
{
    return ui->label_author->text();
}

QString WinRecommendItem::getMusic()
{
    return music;
}

void WinRecommendItem::setSelect(bool select)
{
    if(select){
        ui->widget->setStyleSheet("#widget{background-color:rgb(50,89,206);}");
    }
    else{
        ui->widget->setStyleSheet("#widget{background-color:rgba(128,128,128,198);}");
    }
}

void WinRecommendItem::on_btn_add_clicked()
{
    emit sign_add(music);
}
