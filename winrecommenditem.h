#ifndef WINRECOMMENDITEM_H
#define WINRECOMMENDITEM_H

#include <QWidget>
#include <QMouseEvent>
#include <QListWidgetItem>
namespace Ui {
class WinRecommendItem;
}

class WinRecommendItem : public QWidget
{
    Q_OBJECT

public:
    explicit WinRecommendItem(QWidget *parent = 0);
    ~WinRecommendItem();
    void setInfo(QString music,QString name,QString author);
    void setAddVisible(bool visible);
    QString getName();
    QString getAuthor();
    QString getMusic();
    void setSelect(bool select);
private slots:
    void on_btn_add_clicked();
signals:
    void sign_add(QString music);
private:
    Ui::WinRecommendItem *ui;
    QString music;
};

#endif // WINRECOMMENDITEM_H
