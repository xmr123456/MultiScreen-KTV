#ifndef WINRECOMMENDTITLE_H
#define WINRECOMMENDTITLE_H

#include <QWidget>

namespace Ui {
class WinRecommendTitle;
}

class WinRecommendTitle : public QWidget
{
    Q_OBJECT

public:
    explicit WinRecommendTitle(QWidget *parent = 0);
    ~WinRecommendTitle();

private:
    Ui::WinRecommendTitle *ui;
};

#endif // WINRECOMMENDTITLE_H
